const lotterMaster = require("../models/lotteryPrizeNumberMaster.model");
const timeMaster = require("../models/masterTime.model");
const statusLive = require("../models/statusLive.model");
const lotteryNumber = require("../models/lotteryNumber.model");
const httpStatus = require("http-status");
const { each } = require("bluebird");
const puppeteer = require('puppeteer');

// Test Socket
exports.getlottery = async (req, res, next) => {
  io.emit("Lottery:getNumber", { lotterNumber: Math.random() * 1000 });
  return res.json({});
};

// Use
exports.getLotteryNumberByDatetime = async (req, res, next) => {
  const reqData = req.body;
  res.json({
    success: true,
    data:
      (await lotteryNumber.find({
        date: reqData.date,
        lotteryTime: reqData.time,
      })) || [],
  });
};

exports.getLotteryBetweenByDatetime = async (req, res, next) => {
  const reqData = req.body;
  let dataItem = [];
  let arrStartDate = [];
  arrStartDate = reqData.startDate.split("-");
  let arrEndDate = [];
  arrEndDate = reqData.endDate.split("-");

  rebuildStart =
    arrStartDate[1] +
    "-" +
    parseInt(arrStartDate[0]) +
    "-" +
    (parseInt(arrStartDate[2]) - 543);
  let dateConvertStart = new Date(rebuildStart.toString());
  rebuildEnd =
    arrEndDate[1] +
    "-" +
    parseInt(arrEndDate[0]) +
    "-" +
    (parseInt(arrEndDate[2]) - 543);
  let dateConvertEnd = new Date(rebuildEnd.toString());
  do {
    
    let splitDate = dateConvertStart.toISOString().split("T");
    let arrayDateSplit = splitDate[0].split("-");
    let rebuildDate =
      arrayDateSplit[2] +
      "-" +
      arrayDateSplit[1] +
      "-" +
      (parseInt(arrayDateSplit[0]) + 543);

    let findDataByDate = await lotteryNumber.find({
      date: rebuildDate,
      statusLotteryTime : true,
      $or: [{ prize: 1 }, { prize: 2 }],
    });
    dataItem.push({
      status: findDataByDate.length > 0 ? true : false,
      date: rebuildDate,
      messageData: findDataByDate || [],
    });

    dateConvertStart.setDate(dateConvertStart.getDate() + 1);

  } while (dateConvertEnd >= dateConvertStart);

  res.json({
    success: true,
    data: dataItem,
  });

  // if(reqData){
  //   const dateNow = new Date();
  //   let arrEndDate = []
  //   arrEndDate = reqData.endDate.split("-")
  //   let findDateNow = [{}]

  //   if(arrEndDate[0] == dateNow.getDate() && arrEndDate[1] == (dateNow.getMonth() + 1)){
  //     findDateNow = await lotteryNumber.find({date: reqData.endDate})
  //   }

  //   if(findDateNow.length > 0){
  //     let resultItem = await lotteryNumber.find({date: {
  //       $gte: reqData.startDate,
  //       $lte: reqData.endDate
  //     },$or : [{ prize : 1},{prize : 2}]})

  //     if(findDateNow[0].lotteryNumber != ''){
  //       return res.json({
  //         success: true,
  //         message : {message : 'Date Now Data is Generate' ,status : 1},
  //         data: resultItem,
  //       });
  //     }else{
  //       return res.json({
  //         success: true,
  //         message : {message : 'Date Now Data Number Not Generate' ,status : 2},
  //         data: resultItem,
  //       });
  //     }
  //   }else{
  //     let resultItem = await lotteryNumber.find({date: {
  //       $gte: reqData.startDate,
  //       $lte: reqData.endDate
  //     },$or : [{ prize : 1},{prize : 2}]})

  //     resultItem.push()

  //     return res.json({
  //       success: true,
  //       message : {message : 'Date Now Data Not Generate' ,status : 0},
  //       data: resultItem,
  //     });
  //   }
  // }else{
  //   return res.json({success: true,data : []});
  // }
};
exports.deleteDataMoreTen = async (req, res, next) => {
  const returnData = await lotteryNumber.deleteMany({
    createdAt: {
      $lt: new Date(Date.now() - 20 * 24 * 60 * 60 * 1000).toISOString(),
    },
  });

  res.json({
    success: true,
    data: returnData || [],
  });
};

exports.getLotteryStatusByDatetime = async (req, res, next) => {
  const reqData = req.body;
  const data = (await lotteryNumber.find({ date: reqData.date })) || [];
  let resData = [];

  data.filter((item) => {
    let data = {
      statusLotteryTime: item.statusLotteryTime,
      date: item.date,
      lotteryTime: item.lotteryTime,
    };
    resData.push(data);
  });
  setTimeout(() => {
    res.json({
      success: true,
      data: resData,
    });
  }, 500);
};

exports.getMasterNumberLength = async (req, res, next) => {
  const user = await lotterMaster.find({});
  return res.json({ data: user });
};

exports.getTimeMaster = async (req, res, next) => {
  const user = await timeMaster.find({});
  return res.json({ data: user });
};

exports.setLotteryMaster = async (req, res, next) => {
  try {
    const reqData = req.body;
    let CurrentDate = "";

    for (let i = 0; i < req.body.length; i++) {
      const findItem = await lotteryNumber.find({
        date: req.body[i].date,
        lotteryTime: req.body[i].lotteryTime,
        prize: req.body[i].prize,
        supPrize: req.body[i].supPrize,
      });

      CurrentDate = req.body[i].date;
      if (!findItem.length > 0) {
        const lotteryNumberItem = new lotteryNumber(req.body[i]);
        await lotteryNumberItem.save();
      }
    }

    res.json({
      success: true,
      data: (await lotteryNumber.find({ date: CurrentDate })) || [],
    });
  } catch (error) {
    next(error);
  }
};

exports.getLiveAlready = async (req, res, next) => {
  const reqData = req.body;
  let itemLive = await lotteryNumber
    .find({
      date: reqData.date,
      lotteryTime: reqData.lotteryTime,
      statusLotteryTime: true,
    })
    .sort({ prize: 1, supPrize: 1 });
  if (itemLive) {
    return res.json({ status: true, data: itemLive });
  } else {
    return res.json({ status: false, data: [] });
  }
};

exports.startLottery = async (req, res, next) => {
  const sleep = (milliseconds) => {
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
  };
  if (req.body.length > 0) {
    const reqData = req.body;
    const lotteryLive = new statusLive({
      status: true,
      time: reqData[0].lotteryTime,
    });
    await lotteryLive.save();
    io.emit("Lottery:statusLive", {
      status: true,
      time: reqData[0].lotteryTime,
    });
    var LotteryFind = [{}];
    for (let i = 0; i < reqData.length; i++) {
      if (i == 0) {
        await sleep(900000);
      }

      if (i > 0 && i < reqData.length - 1) {
        await sleep(20000);
      }

      const LotterySet = await lotteryNumber.updateOne(
        {
          date: reqData[i].date,
          lotteryTime: reqData[i].lotteryTime,
          prize: reqData[i].prize,
          supPrize: reqData[i].supPrize,
        },
        { $set: { statusLotteryTime: reqData[i].statusLotteryTime } }
      );
      LotteryFind = await lotteryNumber.find({
        date: reqData[i].date,
        lotteryTime: reqData[i].lotteryTime,
        statusLotteryTime: reqData[i].statusLotteryTime,
      });
      io.emit("Lottery:getNumber", { data: LotteryFind });

      if (i == reqData.length - 1) {
        await sleep(420000);
        const lotteryLive = new statusLive({
          status: false,
          time: reqData[0].lotteryTime,
        });
        await lotteryLive.save();
        let itemLive = await statusLive.find({}).sort({ updatedAt: -1 });
        if (itemLive) {
          io.emit("Lottery:statusLive", {
            status: itemLive[0].status,
            time: reqData[0].lotteryTime,
          });
        }
      }
    }

    return res.json({ status: true, data: LotteryFind });
  } else {
    return res.json({ status: false, data: [] });
  }
};

exports.statusLive = async (req, res, next) => {
  let itemLive = await statusLive.find({}).sort({ updatedAt: -1 });
  if (itemLive.length > 0) {
    //io.emit("Lottery:statusLive", { status : itemLive[0].status });
    return res.json({ status: itemLive[0].status, time: itemLive[0].time });
  } else {
    //io.emit("Lottery:statusLive", { status : false });
    return res.json({ status: false });
  }
};

exports.setLotteryNumberByPrize = async (req, res, next) => {
  if (
    req.body.date &&
    req.body.lotteryTime &&
    req.body.prize &&
    req.body.supPrize &&
    req.body.number != null
  ) {
    const reqData = req.body;

    const LotterySet = await lotteryNumber.updateOne(
      {
        date: reqData.date,
        lotteryTime: reqData.lotteryTime,
        prize: reqData.prize,
        supPrize: reqData.supPrize,
      },
      {
        $set: {
          lotteryNumber:
            reqData.number.toString() + reqData.numberset.toString(),
        },
      }
    );

    const LotteryFind = await lotteryNumber.find({
      date: reqData.date,
      lotteryTime: reqData.lotteryTime,
      prize: reqData.prize,
      supPrize: reqData.supPrize,
    });

    return res.json({ status: true, data: LotteryFind });
  } else {
    return res.json({ status: false, data: [] });
  }
};

exports.setLotteryNumberByPrizeAll = async (req, res, next) => {
  if (req.body.length > 0) {
    const reqData = req.body;
    for (let i = 0; i < req.body.length; i++) {
      const LotterySet = await lotteryNumber.update(
        {
          date: reqData[i].date,
          lotteryTime: reqData[i].lotteryTime,
          prize: reqData[i].prize,
          supPrize: reqData[i].supPrize,
        },
        {
          $set: {
            lotteryNumber:
              reqData[i].number.toString() + reqData[i].numberset.toString(),
          },
        }
      );
    }
    console.log(reqData[0].date);
    const LotteryFind = await lotteryNumber.find({
      date: reqData[0].date,
      lotteryTime: reqData[0].lotteryTime,
    });
    return res.json({ status: true, data: LotteryFind });
  } else {
    return res.json({ status: false, data: [] });
  }
};

exports.getLotteryByDate = async (req, res, next) => {
  const reqData = req.body;

  res.json({
    success: true,
    data: (await lotteryNumber.find({ date: reqData.date })) || [],
  });
};

exports.getLotterDefault = async (req, res, next) => {
  let item = await lotteryNumber
    .find({ statusLotteryTime: true })
    .sort({ updatedAt: -1 });

  if (item[0]) {
    var itemRe = await lotteryNumber
      .find({
        date: item[0].date,
        lotteryTime: item[0].lotteryTime,
        statusLotteryTime: true,
      })
      .sort({ date: 1, prize: 1, subPrize: 1 });
    console.log("itemRe", itemRe);
    var result = [];
    var itemBig = [];
    for (i = 0; i < itemRe.length; i++) {
      if (itemRe[i].prize == 1) {
        itemBig = [];
        itemBig.push(itemRe[i]);
        itemRe.splice(i, 1);
      }
    }

    if (
      itemRe[itemRe.length - 1].prize == 8 &&
      itemRe[itemRe.length - 1].supPrize == 4
    ) {
      itemRe.push(itemBig[0]);
    }

    // io.emit("Lottery:getNumber", { data: itemRe });
    res.json({
      success: false,
      data: itemRe,
    });
  } else {
    res.json({
      success: false,
      data: [],
    });
  }
};
// Not Use
exports.create = async (req, res, next) => {
  try {
        req.body.forEach(async element => {
            const lotterMasterItem = new lotterMaster(element);
            await lotterMasterItem.save();
         });
    res.json({ status: "OK" });
  } catch (error) {}
};

exports.createTime = async (req, res, next) => {
  try {
    req.body.forEach(async element => {
      const timeMasterItem = new timeMaster(element);
      await timeMasterItem.save();
    });
    res.json({ status: "OK" });
  } catch (error) {}
};

exports.getLotterytable = async (req, res, next) => {
  (async () => {
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox']
    });

    const page = await browser.newPage();
    await page.goto('https://huaysong.com/huayhanoy');
    
    const getDimensions = await page.evaluate(() => {
      return {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight
      };
    });
    let datemap = []
    const arrHeader = await page.$$eval('.h1s',nodes => nodes.map(n => n.innerText));
    const arrnumber = await page.$$eval('.table-responsive.secondary',nodes => nodes.map(n => n.innerText));
    
    let fr = 0
    
    for(let i = 0;i < arrHeader.length ;i++){
      let fr = {
              name : arrHeader[i],
              value : {},
            }
      datemap.push(fr)
    }

    remainingArr = datemap.filter(data => data.name != "หวยฮานอย วันนี้ ออกอะไร 22/5/65 จากเซียนดังหลายสำนัก" 
      && data.name != "สูตรหวยฮานอยแม่นๆวันนี้" && data.name != "แนวทางหวยฮานอยวันนี้ออกอะไร" && data.name != "ดูหวยอื่นๆ" );

    datemap = remainingArr
    
    for(let i = 0;i < arrnumber.length ;i++){
      var slice1 = arrnumber[i].split('\n')
      var itemMap = []
      for(let j = 1 ; j < slice1.length; j++){
        var split2 = slice1[j].split('\t')
        var rowsmap = []
        for(let jj = 1 ; jj < split2.length;jj++){
          rowsmap.push(split2[jj])
        }
        itemMap.push({ "mapvalue" : rowsmap})
      }
      console.log("itemMap","--")
      
      datemap[i].value = itemMap
    }
    
    // console.log("datemap",datemap)
  //   for(let i = 0; i < arrHeader.length ;i++){
  //     let fr = {
  //       name : '',
  //       va1 : '',
  //       va2 : '',
  //       va3 : ''
  //     }
  //     fr.name = arrHeader[i]
  //     datemap.push(fr)
  //   }
  //  let v1 = 0
  //  let v2 = 1
  //  let v3 = 2
  //  for(let i = 0;i < datemap.length;i++){
  //     datemap[i].va1 = arrnumber[v1]
  //     datemap[i].va2 = arrnumber[v2]
  //     datemap[i].va3 = arrnumber[v3]

  //     v1 = v1 + 3
  //     v2 = v2 + 3
  //     v3 = v3 + 3
  //  }
    //const getPtag = await page.$eval('.lotto-result', (nodes => nodes.map(n => n.innerText)));
    res.json({
      success: true,
      data: datemap,
    });

    await browser.close();

  //   const page = await browser.newPage();
  //   await page.goto('https://huaysod.me/');
    
  //   const getDimensions = await page.evaluate(() => {
  //     return {
  //       width: document.documentElement.clientWidth,
  //       height: document.documentElement.clientHeight
  //     };
  //   });
  //   let datemap = []
  //   const arrHeader = await page.$$eval('.lotto-col',nodes => nodes.map(n => n.innerText));
  //   const arrnumber = await page.$$eval('.lotto-row .lotto-result',nodes => nodes.map(n => n.innerText));

  //   let fr = 0

  //   for(let i = 0; i < arrHeader.length ;i++){
  //     let fr = {
  //       name : '',
  //       va1 : '',
  //       va2 : '',
  //       va3 : ''
  //     }
  //     fr.name = arrHeader[i]
  //     datemap.push(fr)
  //   }
  //  let v1 = 0
  //  let v2 = 1
  //  let v3 = 2
  //  for(let i = 0;i < datemap.length;i++){
  //     datemap[i].va1 = arrnumber[v1]
  //     datemap[i].va2 = arrnumber[v2]
  //     datemap[i].va3 = arrnumber[v3]

  //     v1 = v1 + 3
  //     v2 = v2 + 3
  //     v3 = v3 + 3
  //  }
  //   //const getPtag = await page.$eval('.lotto-result', (nodes => nodes.map(n => n.innerText)));
  //   res.json({
  //     success: true,
  //     data: datemap,
  //   });

  //   await browser.close();
  })();
};
