const express = require("express");
const validate = require('express-validation');
const controller = require('../../controllers/lottery.controller');
const { authorize, ADMIN, LOGGED_USER } = require("../../middlewares/auth");

const router = express.Router();

// Not use and Test
router.route('/getLottery')
  .get(controller.getlottery)

router.route('/setMasterPrize')
  .post(controller.create)

router.route('/setMasterTime')
  .post(controller.createTime)


// Use
router.route('/getLotteryNumberByDatetime')
  .post(controller.getLotteryNumberByDatetime)

router.route('/getLotteryBetweenByDatetime')
  .post(controller.getLotteryBetweenByDatetime)

router.route('/deleteDataMoreTen')
  .get(controller.deleteDataMoreTen)
  
router.route('/getLotteryStatusByDatetime')
  .post(controller.getLotteryStatusByDatetime)

router.route('/getMasterNumberLength')
  .get(authorize(ADMIN),controller.getMasterNumberLength)

router.route('/getTimeMaster')
  .get(authorize(ADMIN),controller.getTimeMaster)

router.route('/setLotteryMaster')
  .post(controller.setLotteryMaster)

router.route('/getLotteryByDate')
  .post(authorize(ADMIN),controller.getLotteryByDate)

router.route('/startLottery')
  .post(authorize(ADMIN),controller.startLottery)

router.route('/setLotteryNumberByPrize')
  .post(authorize(ADMIN),controller.setLotteryNumberByPrize)

router.route('/getLotterDefault')
  .get(controller.getLotterDefault)

router.route('/setLotteryNumberByPrizeAll')
  .post(authorize(ADMIN),controller.setLotteryNumberByPrizeAll)

router.route('/statusLive')
  .get(controller.statusLive)

router.route('/getLiveAlready')
 .post(controller.getLiveAlready)

router.route('/getLotterytable')
 .get(controller.getLotterytable)
 
module.exports = router;
