const mongoose = require('mongoose');
const httpStatus = require('http-status');

const lotteryPrizeSchema = new mongoose.Schema({
  prize: {
    type: Number,
  },
  numberLength: {
    type : Number
  },
  random: {
    type : Number
  },
  fix: {
    type : String
  },
  countPrize :  {
    type : String
  }
}, {
  timestamps: true,
});

module.exports = mongoose.model('lotteryPrizeNumber', lotteryPrizeSchema);
