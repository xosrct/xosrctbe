const mongoose = require('mongoose');

const statusLive = new mongoose.Schema({
  status: {
    type: Boolean,
  },
  time: {
    type : String
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('statusLive', statusLive);
