const mongoose = require('mongoose');

const lotteryNumberSchema = new mongoose.Schema({
  date: {
    type: String,
  },
  lotteryNumber: {
    type : String
  },
  lotteryTime: {
    type : String
  },
  prize: {
    type : Number
  },
  statusLotteryTime :  {
    type : Boolean
  },
  statusPrize :  {
    type : Boolean
  },
  supPrize :  {
    type : Number
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('lotteryNumber', lotteryNumberSchema);
