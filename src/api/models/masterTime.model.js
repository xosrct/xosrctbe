const mongoose = require('mongoose');
const httpStatus = require('http-status');

const masterTimeSchema = new mongoose.Schema({
  time: {
    type: String,
    
  }
}, {
  timestamps: true,
});

module.exports = mongoose.model('masterTime', masterTimeSchema);
