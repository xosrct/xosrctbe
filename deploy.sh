#!/bin/bash
docker build -t danielfsousa/express-rest-es2017-boilerplate .
docker push danielfsousa/express-rest-es2017-boilerplate

ssh deploy@$DEPLOY_SERVER << EOF
docker pull mongodb+srv://db-mongodb-sgp1-72258-ba9adb36.mongo.ondigitalocean.com
docker stop api-boilerplate || true
docker rm api-boilerplate || true
docker rmi mongodb+srv://db-mongodb-sgp1-72258-ba9adb36.mongo.ondigitalocean.com || true
docker tag mongodb+srv://db-mongodb-sgp1-72258-ba9adb36.mongo.ondigitalocean.come:current
docker run -d --restart always --name api-boilerplate -p 3000:3000 mongodb+srv://db-mongodb-sgp1-72258-ba9adb36.mongo.ondigitalocean.com:current
EOF
